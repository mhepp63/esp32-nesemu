#include "freertos/FreeRTOS.h"
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"
#include "driver/gpio.h"
#include "nofrendo.h"
#include "esp_partition.h"


static RTC_NOINIT_ATTR int lastrom;

char *osd_getromdata() {
	char* romdata;
	const esp_partition_t* part;

	spi_flash_mmap_handle_t hrom;
	esp_err_t err;
	nvs_flash_init();

	part=esp_partition_find_first(0x40, 0x40+lastrom, NULL);	
	if (part==0) printf("Couldn't find rom part!\n");
	err=esp_partition_mmap(part, 0, part->size, SPI_FLASH_MMAP_DATA, (const void**)&romdata, &hrom);
	if (err!=ESP_OK) printf("Couldn't map rom part!\n");
	printf("Initialized. ROM@%p (%s)\n", romdata, part->label);
	return (char*)romdata;
}


esp_err_t event_handler(void *ctx, system_event_t *event)
{
	return ESP_OK;
}


int app_main(void)
{
	esp_reset_reason_t reason = esp_reset_reason();
	if ((reason != ESP_RST_DEEPSLEEP) && (reason != ESP_RST_SW)) {
		printf("not ds and not sw\n");
		lastrom = 0;
	}

	esp_partition_iterator_t it = esp_partition_find(0x40, ESP_PARTITION_SUBTYPE_ANY, NULL);
	int count = 0;
	for (; it != NULL; it = esp_partition_next(it)) {
		const esp_partition_t *p = esp_partition_get(it);
		printf("ROM %d: %s, type: %d, stype: %d, size: %d\n", count, p->label, p->type, p->subtype, p->size);
		++count;
	}

	if ( reason == ESP_RST_SW )
		lastrom = (lastrom+1) % count;

	printf("NoFrendo start with rom subtype %d!\n", 0x40+lastrom);
	nofrendo_main(0, NULL);
	printf("NoFrendo died? WtF?\n");
	asm("break.n 1");
	return 0;
}

