#!/usr/bin/python3

import os
import sys
import tempfile

partlist = [
'nvs,      data, nvs,     0x9000,  0x6000',
'phy_init, data, phy,     0xf000,  0x1000',
'factory,  app,  factory, 0x10000, 0x50000'
]

subtype = 0x40

part_offset = '0x8000'


gen_esp32part = os.environ['IDF_PATH']+'/components/partition_table/gen_esp32part.py'
gen_esp32part_params = [
#    '--flash_size', '4MB',
    '--offset',     part_offset ]

esptool = os.environ['IDF_PATH']+'/components/esptool_py/esptool/esptool.py'
esptool_params = [
    '--chip', 'esp32',
    '--port', '/dev/ttyUSB0',
    '--baud', '921600',
    '--before', 'default_reset',
    '--after', 'hard_reset',
    'write_flash' ]

flash_roms = []

try:
    offset = int(sys.argv[1])
except:
    try:
        offset = int(sys.argv[1][2:], base=16)
    except:
        print("Bad value of offset")

f_roms = sys.argv[2]

f = open(f_roms, 'r')
romlist = f.readlines()
f.close()

for i, rom in enumerate(romlist):
    rom = os.path.expanduser(rom.strip())
    size = os.path.getsize(rom)

    d,m = divmod(size, 4096)
    size = (d + 1 if m else 0) * 4096

    line = 'rom{:02x}, 0x40, 0x{:02x}, 0x{:x}, 0x{:x}'.format(i+1, subtype+i, offset, size)
    flash_roms.append([offset, rom])

    offset += size

    if offset < 4*1024*1024:
        partlist.append(line)
    else:
        print("ROM size exceeded, only {:d} roms added!".format(i))

f_parts_csv = tempfile.NamedTemporaryFile(mode='w', suffix='.csv', prefix='partitions-', delete=True)
f_parts_bin = tempfile.mkstemp(suffix='.bin', prefix='partitions-')

print(">>~~-- List of partitions: --~~<<") 
print()
for l in partlist:
    print(l)
    f_parts_csv.file.write(l+'\n')

f_parts_csv.file.flush()

print()
print(">>~~-- End of partitions: --~~<<") 
print(">> Creating binary partitions map <<")

gen_esp32part_params.append(f_parts_csv.name)
gen_esp32part_params.append(f_parts_bin[1])

mkpart_cmd = '{:s} {:s}'.format(gen_esp32part, ' '.join(gen_esp32part_params))
flash_part_cmd = """{:s} {:s} {:s} '{:s}'""".format(esptool, ' '.join(esptool_params), part_offset, f_parts_bin[1])

print(mkpart_cmd)
res = os.system(mkpart_cmd)
f_parts_csv.close()

if res == 0:

    print(flash_part_cmd)
    res = os.system(flash_part_cmd)

    os.unlink(f_parts_bin[1])

    if res != 0:
        sys.exit(res)

    for flash_rom in flash_roms:
        flash_rom_cmd = """{:s} {:s} 0x{:x} '{:s}'""".format(esptool, ' '.join(esptool_params), flash_rom[0], flash_rom[1])
        print(flash_rom_cmd)
        res = os.system(flash_rom_cmd)

        if res != 0:
            break

sys.exit(res)

