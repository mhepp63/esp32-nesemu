ESP32-NESEMU, a Nintendo Entertainment System emulator for the ESP32
====================================================================

This is a quick and dirty port of Nofrendo, a Nintendo Entertainment System emulator. It lacks sound, but can emulate a NES at close
to full speed, albeit with some framedrop due to the way the display is driven.

Warning
-------

This is a proof-of-concept and not an official application note. As such, this code is entirely unsupported by Espressif.


Compiling
---------

This code is an esp-idf project. You will need esp-idf to compile it. Newer versions of esp-idf may introduce incompatibilities with this code;
for your reference, the code was tested against commit 12caaed28063e32d8b1fb13e13548b6fa52f87b3 of esp-idf.


Display
-------

To display the NES output, please connect a 320x240 ili9341-based SPI display to the ESP32 in this way:

    =====  =======================
    Pin    GPIO
    =====  =======================
    MISO   -
    MOSI   23
    CLK    18
    CS     5
    DC     27
    RST    33
    BCKL   32
    =====  =======================

(BCKL = backlight enable)

Also connect the power supply and ground. For now, the LCD is controlled using a SPI peripheral, fed using the 2nd CPU. This is less than ideal; feeding
the SPI controller using DMA is better, but was left out due to this being a proof of concept.


Controller
----------

To control the NES, you can use cheap Wired USB SNES Controller Retro Gamepad For Nintend. Connect wires directly to buttons. Second pin of buttons connect to GND.

    ======  =====
    Button  GPIO
    ======  =====
    UP      22  
    DOWN    21 
    LEFT    19 
    RIGHT   4 
    MENU    16 
    AUDIO   17 
    SELECT  0 
    START   2 
    A       36 
    B       39 
    ======  =====

Button MENU and AUDIO are X and Y on gamepad. They are serially connected with R button, so you must press R and X or Y. This buttons are used to switch to next ROM and set device into sleep. To wake up use START button.

Sound is on pin 26.

ROM
---

This NES emulator does not come with a ROMs. Please supply your own. This variant support mutiple ROMs. ROMs are flashed one by one based on its size rounded up to 4KB. To flash ROMs use `stack_roms.py` script. This script needs 2 parameters: 0ffset of first ROM and text file with path to ROMs - each line each ROM. Script will create and compile partition table based on ROM set and flash partition table and ROMs to ESP. You can use as much ROM as fits into flash.

After power up device starts on first ROM, each restart switch to next. Wake up from sleep does not switch to next ROM. ROM must be flashed on partition with type set to 0x40 and subtype 0x40+N-1, where N is number of ROM. To store value of actual ROM is used RTC_NOINIT_ATTR variable.

Example use:
-----------

roms.txt:

`
path/to/rom1.nes
path/to/rom2.nes
path/to/rom3.nes
path/to/rom4.nes
path/to/rom5.nes
`

`# stack_roms.py 0x60000 roms.txt`


Copyright
---------

Code in this repository is Copyright (C) 2016 Espressif Systems, licensed under the Apache License 2.0 as described in the file LICENSE. Code in the
components/nofrendo is Copyright (c) 1998-2000 Matthew Conte (matt@conte.com) and licensed under the GPLv2.

